import json
import os
from pathlib import Path

import openreview
from dotenv import load_dotenv
from tqdm import tqdm

from utils import save_file

load_dotenv()  # Loads the .env file that resides in the same directory as this file.

username = os.getenv("USERNAME")
password = os.getenv("PASSWORD")


def get_papers_from(venueid: str):
    # API V2
    client = openreview.api.OpenReviewClient(
        baseurl="https://api2.openreview.net", username=username, password=password
    )
    submissions = client.get_all_notes(content={"venueid": venueid})
    papers = [dict(accepted_submission.content) for accepted_submission in tqdm(submissions)]
    return papers


if __name__ == "__main__":
    Venue_ID = "EMNLP/2023/Conference"
    conf_name = "EMNLP_2023"
    papers = get_papers_from(Venue_ID)

    save_file(Path(f"{conf_name.lower()}_accepted_papers.jsonl"), papers)
