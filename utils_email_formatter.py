import email
import json
import re
from datetime import datetime, timedelta
from pathlib import Path

from utils import get_email_body, save_file

PROTOTYPE_SEP = "------------------------------------------------------------------------------\n"


def json_serial(obj):
    if isinstance(obj, datetime.datetime):
        serial = obj.isoformat()
        return serial


def convert_to_filename_format(date_string):
    # Trim the string
    date_string = date_string.strip()
    # Define the date format without timezone
    date_format = "%a %d %b %y %H:%M:%S"
    # Check if 'GMT' is in the string
    if "GMT" in date_string:
        # Append the timezone format if 'GMT' is found
        date_format += " %Z"
    # Parse the date
    date_obj = datetime.strptime(date_string, date_format)
    return date_obj.strftime("%Y_%m_%d_%H%M")


def get_received_dates(body):
    pattern = r"received from(.*)GMT\n"
    match = re.search(pattern, body)
    if match:
        dates = match.group(1).split("to")
        standard_dates = [convert_to_filename_format(date) for date in dates]
    else:
        now = datetime.now()
        standard_dates = [now + timedelta(days=-1), now]
    return standard_dates


def get_output_file_name(body):
    dates = get_received_dates(body)
    filename = "-".join(dates)
    return filename


def filter_by_line(line):
    return len(line) > 200


def extract_info_from_metadata(text):
    res = dict()
    patterns = {
        "arxiv_id": r"arXiv:(?P<arxiv_id>\d+\.\d+)",
        "date": r"(Date:|replaced with revised version\s*)\s*(?P<date>\w{3},\s\d{1,2}\s\w{3}\s\d{4}\s\d{2}:\d{2}:\d{2}\sGMT)",
        "title": r"Title:\s*(?P<title>.+?)(?=\n[A-Z]|\n\\\\|\n%)",
        "authors": r"Authors:\s*(?P<authors>[^\n]+(?:\n[^\n]+)*?)(?=\n[A-Z]|\n\\\\|\n%)",
        "categories": r"Categories:\s*(?P<categories>.+?)(?=\n|\n\\\\|\n%)",
        "comments": r"Comments:\s*(?P<comments>.+?)(?=\n[A-Z])",
    }
    # Create a list of fields
    fields = ["arxiv_id", "date", "title", "authors", "categories", "comments"]

    # Search for matches for each field in the text
    for field in fields:
        pattern = patterns[field]
        match = re.search(pattern, text, re.DOTALL)
        if match:
            res[field] = match.group(field).strip()
        else:
            res[field] = "Not mentioned"

    return res


def extract_info_from_abs(abstract_txt: str):
    if abstract_txt is None:
        return {"abstract": "Not mentioned"}
    return {"abstract": abstract_txt.strip()}


def extract_info_link(link_str):
    link = re.findall(r"(https?://[^\s]+)", link_str)[0]
    return {"link": link}


def extract_info(item):
    list_item = re.split(r"\\\\[^0-9a-zA-Z]", item)
    # Filter out the empty
    list_item = list(filter(lambda x: len(x) > 0, list_item))

    if len(list_item) == 3:
        meta_data = list_item[0]
        abstract = list_item[1]
        link = list_item[-1]
    else:
        meta_data = list_item[0]
        abstract = None
        link = list_item[-1]

    info_meta = extract_info_from_metadata(meta_data)
    info_abstract = extract_info_from_abs(abstract)
    info_link = extract_info_link(link)
    return {**info_meta, **info_abstract, **info_link}


if __name__ == "__main__":
    # Set proper directories
    email_directory = Path("email_arxiv")
    in_directory = email_directory / "input"
    out_directory = email_directory / "output"  # Corrected the typo here

    # Ensure the output directory exists
    out_directory.mkdir(parents=True, exist_ok=True)

    # Get all the emails in the directory
    email_files = list(in_directory.glob("*.eml"))

    for path_email in email_files:
        body = get_email_body(path_email)

        body_array = body.split(PROTOTYPE_SEP)
        print(len(body_array))
        body_array = list(filter(filter_by_line, body_array))
        print(len(body_array))

        # Transform to dict
        infe = [extract_info(item) for item in body_array]
        infe = [item for item in infe if item is not None]
        save_file(out_directory / f"{get_output_file_name(body)}.json", infe)
