import re

import fitz  # PyMuPDF
import requests


def extract_links_from_pdf(pdf_path):
    # Open the PDF
    doc = fitz.open(pdf_path)

    # Extract links from each page
    links = []
    for page in doc:
        # Get the list of links in the current page
        page_links = page.get_links()
        for link in page_links:
            # Check if it's a URI link
            if link["kind"] == fitz.LINK_URI:
                links.append(link["uri"])

    # Close the PDF document
    doc.close()

    return links


def fetch_arxiv_bibtex_from_links(links):
    arxiv_bibtex_urls = []
    bibtex_contents = []

    # Regular expression to identify arXiv links
    arxiv_link_pattern = re.compile(r"https?://(?:arxiv\.org/abs/|doi\.org/10\.48550/arXiv\.)(\d+\.\d+)")

    # Extract arXiv IDs from the links
    for link in links:
        match = arxiv_link_pattern.search(link)
        if match:
            arxiv_id = match.group(1)
            bibtex_url = f"https://arxiv.org/bibtex/{arxiv_id}"
            arxiv_bibtex_urls.append(bibtex_url)

    # Request the BibTeX entries
    for url in arxiv_bibtex_urls:
        response = requests.get(url)
        if response.status_code == 200:
            bibtex_contents.append(response.text)
        else:
            bibtex_contents.append(f"Error fetching Bibd-TeX for {url}")

    return bibtex_contents


# Example usage
links = extract_links_from_pdf("/home/lepagnol/Downloads/Papier marco NER.pdf")

bibtex_entries = fetch_arxiv_bibtex_from_links(links)
for entry in bibtex_entries:
    print(entry)


# Example usage
# links = extract_links_from_pdf("/home/lepagnol/Documents/These/auto_instruct.pdf")
# print(links)

# Note: This script requires the PyMuPDF (fitz) library.
# You can install it using pip: pip install PyMuPDF
