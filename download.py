import os

import requests
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait


def download_all_papers(base_url, save_dir, driver_path=None):
    driver = webdriver.Firefox()
    driver.get(base_url)

    if not os.path.exists(save_dir):
        os.makedirs(save_dir)

    # wait for the select element to become visible
    wait = WebDriverWait(driver, 10)
    res = wait.until(EC.presence_of_element_located((By.ID, "notes")))
    print("Successful load the website!")
    # parse the results
    divs = driver.find_element(By.CLASS_NAME, "submissions-list")
    num_papers = len(divs)
    for index, paper in enumerate(divs):
        name = paper.find_element(By.CLASS_NAME, "note_content_title").text
        link = paper.find_element(By.CLASS_NAME, "note_content_pdf").get_attribute("href")
        print("Downloading paper {}/{}: {}".format(index + 1, num_papers, name))
        download_pdf(link, os.path.join(save_dir, name))
    driver.close()


def download_pdf(url, name):
    r = requests.get(url, stream=True)

    with open("%s.pdf" % name, "wb") as f:
        for chunck in r.iter_content(1024):
            f.write(chunck)
    r.close()


if __name__ == "__main__":
    ICLR = "https://openreview.net/group?id=ICLR.cc/2023/Conference"
    driver_path = "/home/lepagnol/Documents/These/bibscrapper/chromedriver_linux64/chromedriver"
    save_dir_iclr = "./iclr"

    download_all_papers(ICLR, save_dir_iclr, driver_path)
