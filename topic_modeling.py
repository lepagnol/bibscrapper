#!/usr/bin/env python
# coding: utf-8


# Function that load all json files in a directory and return pd.DataFrame, using pathlib and json packages
# Input: directory path
# Output: pd.DataFrame

import pickle
from pathlib import Path

from bertopic import BERTopic
from bertopic.vectorizers import ClassTfidfTransformer
from hdbscan import HDBSCAN
from sentence_transformers import SentenceTransformer
from sklearn.feature_extraction.text import CountVectorizer
from umap import UMAP

from utils import clean_field, load_json_files

path = Path("/home/lepagnol/Documents/Tools/bibscrapper/email_arxiv/output")
data = load_json_files(path, dedup=True)

## Import Datasets
data["abstract_clean"] = data["abstract"].apply(clean_field)
data["title_clean"] = data["title"].apply(clean_field)

data["title_clean"]
data["docs"] = data["title"] + " \n " + data["abstract"]
docs = data["docs"]


# ## Topic Modeling

# Step 1 - Extract embeddings

# Pre-compute Embeddings
# Prepare embeddings
sentence_model = SentenceTransformer("BAAI/bge-large-en-v1.5")
embeddings = sentence_model.encode(docs.tolist(), show_progress_bar=True)


# Save embeddings as pickle file
with open("embeddings.pickle", "wb") as file:
    pickle.dump(embeddings, file)

# Step 2 - Reduce dimensionality
umap_model = UMAP(n_neighbors=5, n_components=5, min_dist=0.0, metric="cosine")


# Step 3 - Cluster reduced embeddings

cluster_model = HDBSCAN(
    min_cluster_size=25,
    metric="euclidean",
    cluster_selection_method="eom",
    prediction_data=True,
)


# cluster_model = KMeans(n_clusters=10)


# Step 4 - Tokenize topics

vectorizer_model = CountVectorizer(stop_words="english", min_df=3)


# Step 5 - Create topic representation
ctfidf_model = ClassTfidfTransformer(reduce_frequent_words=True)


# All steps together
topic_model = BERTopic(
    umap_model=umap_model,  # Step 2 - Reduce dimensionality
    hdbscan_model=cluster_model,  # Step 3 - Cluster reduced embeddings
    vectorizer_model=vectorizer_model,  # Step 4 - Tokenize topics
    ctfidf_model=ctfidf_model,  # Step 5 - Extract topic words
)


topics, probs = topic_model.fit_transform(docs, embeddings)


# embedding_model = "sentence-transformers/all-MiniLM-L6-v2"
# topic_model.save(
#     "my_model_dir_isk",
#     serialization="safetensors",
#     save_ctfidf=True,
#     save_embedding_model=embedding_model,
# )


# topic_model.save("my_model_topic_hdbscan", serialization="pickle")


dfinfos = topic_model.get_topic_info()
dfinfos.to_pickle("dfinfos.pickle")

topic_model.visualize_heatmap()


topic_model.visualize_barchart()


df_infos = topic_model.get_document_info(docs)
df_infos.to_pickle("doc_infos.pickle")
df_infos.value_counts("Topic")
