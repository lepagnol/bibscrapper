from pathlib import Path

import pandas as pd
import streamlit as st
import streamlit.components.v1 as components
from streamlit_extras.card import card
from streamlit_modal import Modal

from utils import load_json_files


# Streamlit app
def streamlit_app(merged_deduplicated_df):
    st.title("CSV Merger and Deduplicator")
    # Call the function and get the deduplicated DataFrame
    # merged_deduplicated_df = merge_and_deduplicate(json_files)

    # Display the DataFrame
    st.write("Merged and Deduplicated DataFrame:")
    st.dataframe(merged_deduplicated_df)
    # Using columns to create a grid layout
    st.title("Grid of Cards")

    cols = st.columns(3)  # Adjust the number of columns as needed
    for index, item in merged_deduplicated_df.iterrows()[:200]:
        with cols[index % len(cols)]:
            # Call the modified create_card function
            card(title=item["title"], text=item["abstract"], url=item["link"])


# Sample data
data = [
    {"title": "Card 1", "description": "This is the first card.", "url": "https://example.com/card1"},
    {"title": "Card 2", "description": "This is the second card.", "url": "https://example.com/card2"},
    {"title": "Card 3", "description": "This is the third card.", "url": "https://example.com/card3"},
    # Add more items as needed
]


def create_card(title, description, url):
    """Function to create a card with HTML and CSS and display the code to generate it."""
    card_html = f"""
    <a href="{url}" target="_blank" style="text-decoration: none; color: inherit;">
        <div style="margin: 10px; padding: 10px; border-radius: 10px; border: 1px solid #eee; box-shadow: 2px 2px 2px #ccc;">
            <h3>{title}</h3>
            <p>{description}</p>
        </div>
    </a>
    """

    st.markdown(card_html, unsafe_allow_html=True)

    # Button to show/hide the code that generates this card
    if st.button(f"Show code for {title}", key=title):
        st.code(card_html)


# Call the main app function with a DataFrame
# Example: streamlit_app(your_dataframe)


# Run the app
if __name__ == "__main__":
    json_files = list(Path("email_arxiv/output").glob("*.json"))
    path = Path("/home/lepagnol/Documents/Tools/bibscrapper/email_arxiv/output")
    dfs = load_json_files(path, dedup=True)

    streamlit_app(dfs)
