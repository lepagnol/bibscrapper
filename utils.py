import json
import re
from email import policy
from email.parser import BytesParser
from pathlib import Path

import pandas as pd


def clean_field(field_value):
    field_value = re.sub("\s{2,}", " ", field_value)
    return field_value.replace("\n", "").replace("{", "").replace("}", "").strip().lower()


def open_email_file(path):
    with open(path, "rb") as fp:  # select a specific email file from the list
        name = fp.name  # Get file name
        msg = BytesParser(policy=policy.default).parse(fp)
    return msg


def get_email_body(path_email):
    msg = open_email_file(path_email)
    body = msg.get_body(preferencelist=("plain")).get_content()
    return body


def load_json_files(directory: Path, dedup=False):
    # Load all json files in a directory
    json_files = directory.glob("*.json")
    # Create an empty list of dictionaries
    data = []
    # Loop over each json file
    for index, js in enumerate(json_files):
        # Open json file
        with open(js) as json_file:
            # Read json file
            json_text = json.load(json_file)
            # Append json file to list of dictionaries
            data.extend(json_text)
    # Return pd.DataFrame
    arxiv_df = pd.DataFrame(data)
    arxiv_df["date"] = pd.to_datetime(arxiv_df["date"])
    arxiv_df.sort_values(by=["date"], inplace=True)
    if dedup:
        arxiv_df = arxiv_df.drop_duplicates(subset=["arxiv_id"], keep="first")
    return arxiv_df


def save_file(path: Path, data):
    with open(path, "w") as f:
        json.dump(data, f)
