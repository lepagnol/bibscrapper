import bibtexparser
import requests
from bs4 import BeautifulSoup
from mpire import WorkerPool
from tqdm import tqdm


class BibtexFetcher:
    def get_bibtex(self, identifier):
        raise NotImplementedError("Subclasses should implement this method.")


class ArxivFetcher(BibtexFetcher):
    def get_bibtex(self, arxiv_id):
        url = f"https://arxiv.org/bibtex/{arxiv_id}"
        response = requests.get(url)
        if response.status_code == 200:
            return response.text
        else:
            return f"Error: Unable to retrieve BibTeX for ID {arxiv_id} (Status code: {response.status_code})"


class JMLRFetcher(BibtexFetcher):
    def get_bibtex(self, url):
        response_bib = requests.get("https://www.jmlr.org" + url)
        soup_bib = BeautifulSoup(response_bib.text, "html.parser")
        return soup_bib.text


class BibtexManager:
    def __init__(self, fetcher: BibtexFetcher):
        self.fetcher = fetcher

    def fetch_bibtex_for_ids(self, identifiers):
        bibtex_citations = {}
        for identifier in identifiers:
            print(f"Fetching BibTeX for ID: {identifier}")
            bibtex_citations[identifier] = self.fetcher.get_bibtex(identifier)
        return bibtex_citations

    def get_abstract(url):
        response_abstract = requests.get("https://www.jmlr.org" + url)
        soup_abstract = BeautifulSoup(response_abstract.text, "html.parser")
        abstract_text = soup_abstract.find("p", {"class": "abstract"}).text
        return abstract_text


def extract_bibtex_link(page: str):
    page = BeautifulSoup(page, "html.parser")
    links_dict = {tag.text: tag.get("href") for tag in page.find_all("a")}
    return links_dict["bib"]


def get_bib_and_abstract(url):
    # Send a GET request to the URL
    response = requests.get(url)

    # Check if the request was successful
    if response.status_code == 200:
        # Parse the HTML content of the page
        soup = BeautifulSoup(response.text, "html.parser")

        # Find all BibTeX entries (they are inside <pre> tags)
        bibtex_entries = [str(item) for item in soup.find_all("dl")]

        with WorkerPool(n_jobs=6) as pool:
            results = pool.map(infos, bibtex_entries, progress_bar=True)
        # Find all abstracts (they are inside <p> tags with class "abstract")

        return results
    else:
        print("Failed to retrieve the data. Status code:", response.status_code)


if __name__ == "__main__":
    # List of ArXiv IDs you want to fetch BibTeX for
    arxiv_ids = [
        "2403.06988v1",  # Replace with actual ArXiv IDs
        "2305.13971",
    ]

    # Fetch BibTeX citations from ArXiv
    arxiv_fetcher = ArxivFetcher()
    bibtex_manager = BibtexManager(arxiv_fetcher)
    bibtex_citations = bibtex_manager.fetch_bibtex_for_ids(arxiv_ids)

    # Print the results
    for arxiv_id, bibtex in bibtex_citations.items():
        print(f"\nBibTeX for ArXiv ID {arxiv_id}:\n{bibtex}")

    # # URL of the JMLR page with the papers
    # url = "https://www.jmlr.org/papers/v24"
    # path_bib = "jmlr_v24_2023.bib"
    # jmlr_fetcher = JMLRFetcher()
    # library_list = get_bib_and_abstract(url)  # This function may need to be updated to use JMLRFetcher
    # library = bibtexparser.Library(
    #     [bibtexparser.parse_string(item["bib_entry"]).entries[0] for item in library_list]
    # )
    # bibtexparser.write_file(path_bib, library)
    # print(f"BibTeX library has been saved to {path_bib}")
