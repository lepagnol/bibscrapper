# Import necessary libraries
import pandas as pd
import streamlit as st

# Load the DataFrame
df = pd.read_pickle("doc_infos.pickle")
# Title for the dashboard
st.title("Dataframe Dashboard with Filters")

# Sidebar for filter options
st.sidebar.header("Filter Options")

# Select a column to filter
column_to_filter = st.sidebar.selectbox("Select the column to filter", df.columns)

# Select values from the chosen column
unique_values = df[column_to_filter].unique()
selected_value = st.sidebar.selectbox("Filter by:", unique_values)

# Filter the DataFrame
filtered_df = df[df[column_to_filter] == selected_value]

# Display the filtered DataFrame
st.write("Filtered Data")
st.dataframe(filtered_df)
